// This is just an example,
// so you can safely delete all default props below

export default {
  failed: 'Action failed',
  success: 'Action was successful',
  label:{
    mobile_number: 'Mobile Number',
    proceed: 'Proceed',
    have_a_token: 'Already Have a token?',
    go_to_home: 'Go to Home?',
    token: 'Token',
    resend:'Resend'
  },
  validation:{
    required:'Required'
  },
  button: {
    back:'Back',
  }
}
